/**
 * For starting - create car with consumption:
 * var car = Car(5);
 * 3 cars added - lexus, daewoo, niva
 * Car module. Created car with custom consumption.
 * @param consumption {number}
 * @return {{
 *            start: start,
*             riding: riding,
 *            checkGas: checkGas,
 *            checkOil: checkOil;
  *           refuelingGas: refuelingGas,
  *           refuelOil: refuelOil,
  *           setSpeed: setSpeed,
  *           checkSpeed: checkSpeed
*           }}
 * @constructor
 */
function Car(consumption, oilConsumption, maxSpeed) {
  if (!isNumeric(consumption)) {
    showMessage('Wrong consumption', 'error');
    return;
  }

  if (!isNumeric(oilConsumption)) {
    showMessage('Wrong consumption', 'error');
    return;
  }

  if (!isNumeric(maxSpeed)) {
    showMessage('Wrong consumption', 'error');
    return;
  }


  let gasBalance = 100;
  let oilBalance = 1;
  let gasConsumption = consumption / 100;
  let oilWaste = oilConsumption / 1000;
  let gasResidue;
  let oilResidue;
  let gasVolume = 200;
  let oilVolume = 10;
  let ignition = false;
  let ready = false;
  let carMaxSpeed = maxSpeed;
  let carSpeed = 60;

  /**
	 * Show message for a user in the console.
	 * @param message {string} - Text message for user.
	 * @param type {string} - Type of the message (error, warning, log). log by default.
	 */
  function showMessage(message, type) {
    let messageText = message ? message : 'Error in program. Please call - 066 083 07 06';

    switch (type) {
      case 'error':
        console.error(messageText);
        break;
      case 'warning':
        console.warn(messageText);
        break;
      case 'log':
        console.log(messageText);
        break;
      default:
        console.log(messageText);
        break;
    }
  }

  /**
	 * Check gas amount after riding.
	 * @param distance {number} - Riding distance.
	 * @return {number} - Gas amount.
	 */
  function gasCheck(distance) {
    if (gasBalance <= 0) {
      return 0;
    }

    let gasForRide = Math.round(distance * gasConsumption);

    return gasBalance - gasForRide;
  }
  /**
	 * Check oil amount after riding.
	 * @param distance {number} - Riding distance.
	 * @return {number} - oil amount.
	 */

  function oilCheck(distance) {
    if(oilBalance <= 0) {
      return 0;
    }
    let oilForRide = distance * oilWaste;

    return oilBalance - oilForRide;
  }

  /**
	 * Check car for ride.
	 * @param distance {number} - Ride distance.
	 */

  function checkRide(distance) {
    gasResidue = gasCheck(distance);
    oilResidue = oilCheck(distance);
  }

  

  /**
	 * Check value to number.
	 * @param n {void} - value.
	 * @return {boolean} - Is number.
	 */

  function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
  
  /**
	 * Public methods object.
	 */
  return {
    /**
		 * Start car.
		 */
    start: function() {
      ignition = true;

      if (gasBalance <= 0) {
        showMessage('You don\'t have gas. Please refuel the car.', 'error');
        ready = false;
        return;
      }

      if (oilBalance <= 0) {
        showMessage('You don\'t have oil. Please refuel the car.', 'error');
        ready = false;
        return;
      }

      ready = true;

      showMessage('Ingition', 'log');
    },


    /**
		 * Riding function.
		 * @param distance {number} - Riding distance.
     * @param carSpeed {number} - Riding speed.
		 */
    riding: function(distance) {
      if (!isNumeric(distance)) {
        showMessage('Wrong distance', 'error');
        return;
      }

      if (!ignition) {
        showMessage('You need start car', 'error');
        return;
      }

      if (!ready) {
        ignition = false;
        showMessage('You need gas station', 'error');
        return;
      }
      
      checkRide(distance);

      let time = Math.round(distance / carSpeed * 100) / 100;


      let gasDriven = gasBalance / (gasConsumption * 100) * 100;
      let oilDriven = oilBalance / (oilWaste * 100) * 100;

      if(gasResidue <= 0 && oilResidue <= 0 ) {

        gasBalance = 0;
        oilBalance = 0;

        let distanceLeft = Math.round(distance - gasDriven);

        let needdedGas = distanceLeft * gasConsumption;
        let needdedOil = distanceLeft * oilWaste;
        
        time = Math.round(gasDriven / carSpeed * 100) / 100;

        showMessage('Gas and Oil over. You have driven - '+ gasDriven + ' km. You need ' + needdedGas + ' gas liters and '+ needdedOil + ' oil liters. ' + distanceLeft + ' km left. You spent '+ time + ' hours', 'warning');
      } else if(gasResidue > 0 && oilResidue <= 0) {

        
        oilBalance = 0;

        let distanceLeft = Math.round(distance - oilDriven);

        gasBalance = gasBalance - (oilDriven * gasConsumption);

        let needdedOil = distanceLeft * oilWaste;

        time = Math.round(oilDriven / carSpeed * 100) / 100;
        showMessage('Oil over. You have driven - '+ oilDriven + ' km. You need '+ needdedOil + ' oil liters. ' + distanceLeft + ' km left.  You spent '+ time + ' hours', 'warning');

      } else if(gasResidue <= 0 && oilResidue > 0) {
       
        gasBalance = 0;

        let distanceLeft = Math.round(distance - gasDriven);
        
        oilBalance = oilBalance - (gasDriven * oilWaste);
        console.log(oilBalance);

        let needdedGas = distanceLeft * gasConsumption;

        time = Math.round(gasDriven / carSpeed * 100) / 100;

        showMessage('Gas over. You have driven - '+ gasDriven + ' km. You need ' + needdedGas + ' gas liters. ' + distanceLeft + ' km left. You spent '+ time + ' hours', 'warning');
      } else {
        gasBalance = gasResidue;
        oilBalance = oilResidue;
        showMessage('You arrived. Gas balance - ' + gasResidue + '. Oil balance - ' + oilResidue + '. Time you spent - ' + time + ' hours', 'log');
      }

    

    },

    /**
		 * Check gas function.
		 */
    checkGas: function() {
      showMessage('Gas - ' + gasBalance, 'log');
    },

    /**
		 * Check oil function.
		 */

    checkOil: function() {
      showMessage('Oil - ' + oilBalance, 'log');
    }, 

     /**
		 * Set speed function.
     * @param speed {number}
		 */

    setSpeed: function(speed){
      if (!isNumeric(speed)) {
        showMessage('Wrong gas amount', 'error');
        return;
      }

      if(speed > carMaxSpeed){
        showMessage('Speed can not be bigger then car\'s maxSpeed', 'warning');
        return;
      } else {
        carSpeed = speed;
        showMessage('Speed now  - ' + carSpeed, 'log');
      }
    },

     /**
		 * Check speed function.
		 */

    checkSpeed: function(){
      showMessage('Car\'s speed now ' + carSpeed, 'log');
    },

    /**
		 * refuelingGas function.
		 * @param gas
		 */
    refuelingGas: function(gas) {
      if (!isNumeric(gas)) {
        showMessage('Wrong gas amount', 'error');
        return;
      }

      if (gasVolume === gasBalance) {
        showMessage('Gasoline tank is full', 'warning');
      } else if (gasVolume < (gasBalance + gas)) {
        gasBalance = 200;
        let excess = Math.round((gas - gasBalance) * 100) / 100;
        showMessage('Gasoline tank is full. Excess - ' + excess, 'log');
      } else {
        gasBalance = Math.round(gasBalance + gas);
        showMessage('Gas balance - ' + gasBalance, 'log');
      }
    },

    refuelingOil: function(oil) {
      if (!isNumeric(oil)) {
        showMessage('Wrong oil amount', 'error');
        return;
      }
      
      if (oilVolume === oilBalance) {
        showMessage('Gasoline tank is full', 'warning');
      } else if (oilVolume < oilBalance + oil) {
        oilBalance = 10;
        let excess = Math.round(oil - oilBalance);
        showMessage('Oil tank is full. Excess - ' + excess, 'log');
      } else {
        oilBalance = Math.round(oilBalance + oil);
        showMessage('Oil balance - ' + oilBalance, 'log');
      }

    },
  };
}

let lexus = new Car(60, 2, 180);
let daewoo = new Car(30, 3, 130);
let niva = new Car(45, 4, 100);